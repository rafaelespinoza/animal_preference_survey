Rails.application.routes.draw do
  root to: 'guesses#new'
  resources :guesses, only: %i[show create update]
end
