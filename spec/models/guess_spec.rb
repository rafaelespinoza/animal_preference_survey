require 'rails_helper'

RSpec.describe Guess, type: :model do
  before(:each) do
    %w[dog cat].each { |name| Animal.create!(name: name) }
    animal_id_map = Animal.id_map
    Aggregation.seed_from_csv(animal_id_map)
  end

  describe '.create_from_http_params' do
    context 'valid input' do
      let(:params) do
        request_body = ({ weight: 1, height_in_feet: 1, height_in_inches: 1 }).to_json
        new_http_params(request_body)
      end

      it 'creates a Guess' do
        expect { described_class.create_from_http_params(params) }
          .to change { described_class.count }.by(1)
      end

      it 'returns a Guess object' do
        actual = described_class.create_from_http_params(params)
        expect(actual.valid?).to be_truthy
        expect(actual.persisted?).to be_truthy
      end
    end

    context 'invalid input' do
      let(:params) do
        request_body = ({ weight: 0, height_in_feet: 0, height_in_inches: 0 }).to_json
        new_http_params(request_body)
      end

      it 'does not create a Guess' do
        expect { described_class.create_from_http_params(params) }
          .not_to change { described_class.count }
      end
    end

    private

    def new_http_params(request_body)
      attrs = JSON.parse(request_body)
      ActionController::Parameters.new(attrs).permit!
    end
  end

  describe '#height' do
    it 'is invalid when < 1' do
      guess = described_class.create(animal_id: 1, weight: 10, height: 0)
      guess.valid?
      expect(guess.errors[:height]).not_to be_empty
    end
  end

  describe '#weight' do
    it 'is invalid when < 1' do
      guess = described_class.create(animal_id: 1, weight: 0, height: 10)
      guess.valid?
      expect(guess.errors[:weight]).not_to be_empty
    end
  end

  describe '#bmi!' do
    it 'assigns correct values' do
      guess = described_class.create(height: 100, weight: 100)
      expect(guess.bmi).to be_within(0.1).of(7.0)

      guess = described_class.create(height: 100, weight: 200)
      expect(guess.bmi).to be_within(0.1).of(14.0)

      guess = described_class.create(height: 100, weight: 300)
      expect(guess.bmi).to be_within(0.1).of(21.0)

      guess = described_class.create(height: 25, weight: 100)
      expect(guess.bmi).to be_within(0.1).of(112.5)

      guess = described_class.create(height: 50, weight: 100)
      expect(guess.bmi).to be_within(0.1).of(28.1)

      guess = described_class.create(height: 75, weight: 100)
      expect(guess.bmi).to be_within(0.1).of(12.5)
    end
  end

  describe '#animal!' do
    xit 'assigns correct values' do
    end
  end
end
