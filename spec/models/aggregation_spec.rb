require 'rails_helper'

RSpec.describe Aggregation, type: :model do
  describe '.aggregate' do
    let(:tuples) do
      [
        ['aardvark',  10, 10],
        ['boar',      20, 20],
        ['boar',      25, 25],
        ['cat',       30, 30],
        ['cat',       35, 35],
        ['cat',       40, 40],
        ['dog',       50, 50],
        ['dog',       60, 60],
        ['dog',       70, 70],
        ['dog',       80, 80],
      ]
    end

    let(:animal_id_map) do
      {
        'aardvark' => 1,
        'boar' => 2,
        'cat' => 3,
        'dog' => 4,
      }
    end

    it 'returns correct result' do
      actual = described_class.send(:aggregate, tuples, animal_id_map)
      expected = ActiveSupport::HashWithIndifferentAccess.new(
        aardvark: { animal_id: 1, count: 1, total_height: 10, total_weight: 10 },
        boar:     { animal_id: 2, count: 2, total_height: 45, total_weight: 45 },
        cat:      { animal_id: 3, count: 3, total_height: 105, total_weight: 105 },
        dog:      { animal_id: 4, count: 4, total_height: 260, total_weight: 260 },
      )

      expect(actual).to eq(expected)
    end
  end
end
