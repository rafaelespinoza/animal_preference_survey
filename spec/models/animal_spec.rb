require 'rails_helper'

RSpec.describe Animal, type: :model do
  describe '.id_map' do
    before(:each) do
      %w[aardvark boar cat dog].each { |name| Animal.create!(name: name) }
    end

    it 'returns Hash with these keys' do
      actual = described_class.id_map
      expect(actual.keys).to eq(%w[aardvark boar cat dog])
    end

    it 'returns Hash with ids of corresponding animals' do
      actual = described_class.id_map
      actual.each_pair do |animal_name, animal_id|
        animal = Animal.find(animal_id)
        expect(animal.name).to eq(animal_name)
      end
    end
  end

  describe '#name' do
    it 'saves name in lower case' do
      animal = described_class.create(name: 'FOO')
      expect(animal.persisted?).to be_truthy
      expect(animal.name).to eq('foo')
    end
  end
end
