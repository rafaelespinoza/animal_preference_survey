require 'rails_helper'

RSpec.describe "Usages", type: :request do
  before(:each) do
    %w[dog cat].each { |name| Animal.create!(name: name) }

    animal_id_map = Animal.id_map
    Aggregation.seed_from_csv(animal_id_map)
  end

  describe "User's initial inputs height, weight and creating a Guess" do
    it "creates, updates guess record" do
      params = { height_in_feet: 8, height_in_inches: 4, weight: 400 }
      post('/guesses', params: params, as: :json)

      # check response
      expect(response).to have_http_status(:created)
      resp_body = JSON.parse(response.body)
      expect(resp_body.keys).to match_array(%w[id height weight bmi correct animal_id created_at updated_at])

      # check db effects
      guess = Guess.find(resp_body['id'])
      expect(guess).to have_attributes(
        height: 100,
        weight: 400,
        bmi: 28.12,
        correct: nil,
        animal: Animal.find_by(name: 'cat'),
      )

      # now, update the record
      params = { correct: true }
      put("/guesses/#{guess.id}", params: params, as: :json)

      expect(response).to have_http_status(:ok)
      resp_body = JSON.parse(response.body)
      guess = Guess.find(resp_body['id'])
      expect(guess).to have_attributes(correct: true)
    end
  end
end
