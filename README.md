# README

## what

A Rails app that takes in a person's height and weight and then guesses if that
person likes dogs or cats better.

#### application requirements

- Be able to save guesses with each use
- Confirm with the user if your guess was correct
- Guesses should improve with over time with use

## how

```sh
# install deps
bundle install

# set up db
./lib/tasks/db.sh

# start server
bundle exec rails s

# run tests
bundle exec rspec
```

---

Something I've wanted to add to this app is to use statistics to improve the
favorite animal guesses. One strategy for doing this is to compare a new set of
inputs to a set of pre-calculated standard deviations per animal and then choose
the animal which has an average closest to the set of inputs and has the lowest
standard deviation.

A standard deviation is used to measure how spread out a group of numbers are
from the average. So, if the height & weight inputs are close to an average for
a preferred animal, and there is low variance of previously-recorded heights &
weights that map to a preferred animal, then there's a good chance that we could
predict somebody's favorite animal based on the height & weight inputs.

Which input are we calculating the standard deviation on? It's not height, it's
not weight, but instead I've chosen to reduce it into one value: BMI. The reason
for this is that it's easier to deal with one dimension, instead of two.
According to this [website](http://www.whathealth.com/bmi/formula.html) the
formula for calculating BMI from weight in pounds & height in inches is:

     (weight in pounds * 703) / (height_in_inches^2)

The website [mathisfun](https://www.mathsisfun.com/data/standard-deviation.html)
defines standard variation as:

    the square root of the average of squared differences from the mean

The steps for doing so would be:

- Group together all preferred animal entries (dogs w/ dogs, cats w/ cats, etc)
- For each group of animal lovers:
  - get the mean BMI
  - For each entry:
    - calculate the `BMI - mean`
    - square the difference
  - sum up each squared difference
  - get the mean of that sum

Periodically we'd have to re-measure the BMI's standard deviation, which would
be an expensive process. But perhaps that can be relegated to a background job.
