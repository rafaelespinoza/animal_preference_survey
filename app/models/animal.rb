class Animal < ApplicationRecord
  validates :name, presence: true

  before_save :lower_case_name!

  def self.id_map
    all.order(:name)
       .pluck(:name, :id)
       .each_with_object({}) { |tuple, map| map[tuple[0]] = tuple[1] }
  end

  private

  def lower_case_name!
    @name = name.downcase!
  end
end
