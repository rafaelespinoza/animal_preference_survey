class Guess < ApplicationRecord
  belongs_to :animal

  validates :animal, presence: true
  validates :weight, numericality: { greater_than: 0 }
  validates :height, numericality: { greater_than: 0 }

  before_validation :bmi_animal!, on: :create

  def self.create_from_http_params(params)
    feet = params[:height_in_feet].to_i
    inches = params[:height_in_inches].to_i
    height = (feet * 12) + inches

    attrs = params.reject { |key| %w[height_in_feet height_in_inches].include?(key) }
                  .merge(height: height)

    create(attrs)
  end

  def height_in_feet_inches
    { feet: height_in_feet, inches: height_in_inches }
  end

  def height_in_feet
    height / 12
  end

  def height_in_inches
    height % 12
  end

  private

  def bmi_animal!
    bmi!
    animal!
  end

  # calculation based on: http://www.whathealth.com/bmi/formula.html
  def bmi!
    w = weight.to_f
    h = height.to_f
    bmi = (w * 703.0) / (h * h)
    self.bmi = bmi.round(2)
  end

  def animal!
    timestamp = Time.current - 1.day # TODO: Pick last time an agg was run.
    animal_aggs = Aggregation.grouped_by_animal_created_since(timestamp)

    raise StandardError, 'no animal aggregation data found!' if animal_aggs.length < 1

    min_dist = (animal_aggs[0].average_bmi - bmi).abs
    guessed_animal = animal_aggs[0].animal_id

    for i in (1...animal_aggs.length) do
      dist = (animal_aggs[i].average_bmi - bmi).abs
      if dist < min_dist
        min_dist = dist
        guessed_animal = animal_aggs[i].animal_id
      end
    end

    self.animal_id = guessed_animal
  end
end
