# frozen_string_literal: true

require 'csv'

class Aggregation < ApplicationRecord
  SEED_FILE = Rails.root.join("db/AnimalHeightWeight.csv")

  belongs_to :animal
  validates :count, numericality: { greater_than: 0 }

  class << self
    def test
      Aggregation.all.each do |agg|
        pp agg.animal.name
        pp agg
        pp agg.stats
      end
    end

    def seed_from_csv(animal_id_map)
      tuples = parse_seed_csv
      attrs_by_animal = aggregate(tuples, animal_id_map)
      attrs_by_animal.each_value { |attrs| create!(attrs) }
    end

    # Based on: https://stackoverflow.com/a/7630564/2908123
    def grouped_by_animal_created_since(timestamp)
      projection = <<~SQL
        DISTINCT(animals.name),
        animals.name AS animal_name,
        aggregations.id,
        animal_id,
        total_height,
        total_weight,
        count,
        aggregations.created_at
      SQL

      joins(:animal)
        .select(projection)
        .where('aggregations.created_at >= ?', timestamp)
        .order('animals.name', 'aggregations.created_at DESC', 'aggregations.id')
        .group('animals.name, aggregations.id')
    end

    private

    def parse_seed_csv
      tuples = CSV.read(
        SEED_FILE,
        headers: true
      )

      tuples.map do |tup|
        [
          tup[0].downcase,  # Animal
          tup[1].to_i,      # Height (inches)
          tup[2].to_i,      # Weight (pounds)
        ]
      end
    end

    def aggregate(tuples, animal_id_map)
      agg_by_animal = tuples.each_with_object({}) do |row, agg_by_animal|
        animal = row[0]

        if agg_by_animal.key?(animal)
          curr = agg_by_animal[animal]

          agg_by_animal[animal] = {
            animal_id:    curr[:animal_id],
            count:        curr[:count] + 1,
            total_height: curr[:total_height] + row[1],
            total_weight: curr[:total_weight] + row[2]
          }
        else
          agg_by_animal[animal] = {
            animal_id:    animal_id_map[animal],
            count:        1,
            total_height: row[1],
            total_weight: row[2],
          }
        end
      end

      ActiveSupport::HashWithIndifferentAccess.new(agg_by_animal)
    end
  end

  def stats
    {
      average_bmi:        average_bmi,
      average_height:     average_height,
      average_weight:     average_weight,
    }
  end

  def average_bmi
    w = average_weight
    h = average_height
    bmi = (w * 703.0) / (h * h)
    bmi.to_f.round(2)
  end

  def average_height
    h = total_height.to_f
    c = count.to_f
    (h / c).round(2)
  end

  def average_weight
    w = total_weight.to_f
    c = count.to_f
    (w / c).round(2)
  end
end
