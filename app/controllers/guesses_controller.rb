class GuessesController < ApplicationController
  attr_reader :guess
  before_action :guess!, except: :create

  def show
    if guess.present?
      render json: guess
    else
      render status: :not_found
    end
  end

  def create
    @guess = create_guess

    if guess.persisted?
      render json: guess, status: :created
    else
      render_error(guess)
    end
  end

  def update
    unless guess.present?
      render status: :not_found
      return
    end

    if guess.update(guess_params)
      render json: guess, status: :ok
    else
      render_error(guess)
    end
  end

  private

  def guess_params
    paramify_json_body.permit(
      :height,
      :height_in_feet,
      :height_in_inches,
      :weight,
      :correct,
      :animal_id,
    )
  end

  def guess!
    id = params[:id]
    @guess = Guess.find_by(id: id)
  end

  def create_guess
    Guess.create_from_http_params(guess_params)
  end
end
