class ApplicationController < ActionController::API

  private

  def render_error(model_instance)
    render json: model_instance.errors, status: :unprocessable_entity
  end

  def paramify_json_body
    body = request.body.read
    attr = JSON.parse(body)
    ActionController::Parameters.new(attr)
  end
end
