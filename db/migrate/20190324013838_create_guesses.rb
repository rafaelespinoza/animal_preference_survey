class CreateGuesses < ActiveRecord::Migration[5.2]
  def change
    create_table :guesses do |t|
      t.integer :height
      t.integer :weight
      t.float   :bmi
      t.boolean :correct
      t.references :animal, foreign_key: true

      t.timestamps
    end

    add_index :guesses, :created_at
  end
end
