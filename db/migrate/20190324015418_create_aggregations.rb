class CreateAggregations < ActiveRecord::Migration[5.2]
  def change
    create_table :aggregations do |t|
      t.references :animal, foreign_key: true
      t.integer :total_height
      t.integer :total_weight
      t.integer :count

      t.timestamps
    end

    add_index :aggregations, :created_at
  end
end
