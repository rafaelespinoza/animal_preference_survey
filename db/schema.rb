# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_24_015418) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "aggregations", force: :cascade do |t|
    t.bigint "animal_id"
    t.integer "total_height"
    t.integer "total_weight"
    t.integer "count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["animal_id"], name: "index_aggregations_on_animal_id"
    t.index ["created_at"], name: "index_aggregations_on_created_at"
  end

  create_table "animals", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_animals_on_name", unique: true
  end

  create_table "guesses", force: :cascade do |t|
    t.integer "height"
    t.integer "weight"
    t.float "bmi"
    t.boolean "correct"
    t.bigint "animal_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["animal_id"], name: "index_guesses_on_animal_id"
    t.index ["created_at"], name: "index_guesses_on_created_at"
  end

  add_foreign_key "aggregations", "animals"
  add_foreign_key "guesses", "animals"
end
