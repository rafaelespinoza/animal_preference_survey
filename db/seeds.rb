# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def initialize_data
  %w[dog cat].each { |name| Animal.create!(name: name) }
  animal_id_map = Animal.id_map
  Aggregation.seed_from_csv(animal_id_map)
end

initialize_data
