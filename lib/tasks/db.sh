#!/usr/bin/env bash

set -u -o pipefail

function reset () {
  bundle exec rails db:drop
  bundle exec rails db:create
  bundle exec rails db:migrate
  bundle exec rails db:seed
}

reset
